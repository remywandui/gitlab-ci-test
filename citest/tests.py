from django.test import LiveServerTestCase
from django.contrib.auth.models import User
from django.contrib.staticfiles.testing import StaticLiveServerTestCase

from selenium import webdriver
from selenium import common

import datetime

class LoginTestCase(StaticLiveServerTestCase):
    '''
    Test if it's possible for a user
    to login through the admin page
    '''
    def setUp(self):
        '''
        Instantiate and configure selenium webdriver
        and other requirements for testing
        '''
        # browserstack configuration details
        ACCESS_NAME = 'jeremywandui1'
        ACCESS_KEY = 'Qkd1D3tFiRVxdYZ12ZvV'
        stamp = datetime.date.strftime(datetime.datetime.now(), '%Y.%m.%d.%H.%M.%S')
        desired_cap = {
            'os': 'Windows',
            'os_version':'10',
            'browser':'Chrome',
            'browser_version':'53.0',
            'resolution':'1920x1080',
            'build':'reject-1.0.{}'.format(stamp),
            'browserstack.debug':'true',
            'browserstack.local':'true'
        }

        executor = 'http://{0}:{1}@hub.browserstack.com:80/wd/hub'.format(ACCESS_NAME, ACCESS_KEY)

        self.selenium = webdriver.Remote(
            command_executor=executor,
            desired_capabilities=desired_cap
        )
        self.selenium.maximize_window()

        # Login credentials for admin
        self.username = 'admin'
        self.password = 'djangoadmin2!'

        User.objects.create_superuser(
            username = self.username, 
            password = self.password,
            email = 'admin@example.com'
        )

    def tearDown(self):
        '''
        Ensure graceful shutdown of 
        selenium and the test environment
        '''
        self.selenium.quit()

    def test_login(self):
        '''
        Django admin login user test
        
        This will attempt to log in an already
        existing user into the django admin site.
        '''
        # Open django admin page.
        # DjangoLiveServerTestCase provides a live server
        # url to access the base url in tests
        self.selenium.get(
                '{}/{}'.format(self.live_server_url, 'admin/')
        )

        # Fill login information
        self.selenium.find_element_by_id('id_username').send_keys(self.username)
        self.selenium.find_element_by_id('id_password').send_keys(self.password)

        # Find login button and click on it
        try:
            self.selenium.find_element_by_xpath('//input[@value="Log in"]').submit()
            self.assertIn('admin', self.selenium.title)
        except common.exceptions.NoSuchElementException:
            return False
